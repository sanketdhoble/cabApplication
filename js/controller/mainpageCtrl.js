testApp.controller("mainpageCtrl",function($scope,$window,$http,$timeout) {
      
      $scope.flag=true;
      $scope.value=false;
      //Date time Picker
       var today = new Date();
        $scope.StartDate = new Date();
        $scope.EndDate = new Date();
        $scope.dateFormat = 'dd-MM-yyyy';
        $scope.StartDateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            // minDate: today,
            maxDate: new Date(2020, 5, 22)
        };
        $scope.EndDateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            // minDate: today,
            maxDate: new Date(2020, 5, 22)
        };
        $scope.StartDatePopup = {
            opened: false
        };
        $scope.EndDatePopup = {
            opened: false
        };
         $scope.EndDate = "";
         $scope.StartDate= "" ;
        $scope.ChangeEndMinDate = function(StartDate) {
            if (StartDate != null) {
                var expiryMinDate = new Date(StartDate);
                $scope.EndDateOptions.minDate = expiryMinDate;
               /* $scope.EndDate = expiryMinDate;*/
               if($scope.EndDate<expiryMinDate)
               {
                 $scope.EndDate = "";
               }
               
            }
        };
        /*$scope.ChangeExpiryMinDate();*/
        $scope.OpenStartDate = function() {
            $scope.StartDatePopup.opened = !$scope.StartDatePopup.opened;
           
            if($scope.StartDate==""&&$scope.value==true)
              $scope.flag=false;
            
             if($scope.StartDate=="")
               $scope.value=true;
        
            else if($scope.StartDate!="")
              $scope.flag=false;
          
        };
        $scope.OpenEndDate = function() {
            $scope.EndDatePopup.opened = !$scope.EndDatePopup.opened;
            if($scope.EndDate!=""&&$scope.value==true)
              $scope.flag=false;
            
            if($scope.EndDate==""&&$scope.value==true)
               $scope.flag=false;

            if($scope.EndDate=="")
              $scope.value=true;

        };

        $scope.timeRange=function()
        {
          $scope.getFunction();
            console.log($scope.transferData);
            if($scope.EndDate=="")
            {
              $scope.EndDate=new Date();
            }
            //Calculated timings separately in order to show data of that date irrespective of its time (as 10am on same day is not valid for same start day with time 11am).
             $timeout(function () {
                      
            $scope.startDate_year=$scope.StartDate.getFullYear();
            $scope.startDate_month=$scope.StartDate.getMonth()+1;
            $scope.startDate_date=$scope.StartDate.getDate();
            $scope.endDate_year=$scope.EndDate.getFullYear();
            $scope.endDate_month=$scope.EndDate.getMonth()+1;
            $scope.endDate_date=$scope.EndDate.getDate();
            $scope.startDateValue=$scope.startDate_year*12*365+$scope.startDate_month*12+$scope.startDate_date;
            $scope.endDateValue=$scope.endDate_year*12*365+$scope.endDate_month*12+$scope.endDate_date;
            for(var i=0;i<$scope.getData.length;i++)
            {
              $scope.date = new Date($scope.getData[i].join_date);
              $scope.join_date = $scope.date.getDate();
              $scope.join_year=$scope.date.getFullYear();
              $scope.join_month=$scope.date.getMonth()+1;
              $scope.joinValue=$scope.join_year*12*365+$scope.join_month*12+$scope.join_date;
              if(!($scope.joinValue>=$scope.startDateValue&&$scope.joinValue<=$scope.endDateValue))
              {
                index=i;
                $scope.getData.splice(index,1);
                i--;
              } 

            }
            console.log($scope.getData);

            },200);
        }

      //END//

      $scope.flagFriends=true;
      $scope.flagChirpiness=true;
      $scope.flagInfluence=true;
      $scope.flagTotal=true;
      $scope.getFunction=function()
      {

        $http.get('pages/sample.json')
        .then(function (response) {
          $scope.getData = response.data;
        
          })
        
      }
      $scope.getFunction();

      $scope.delete=function(index)
      {
        $scope.getData.splice(index, 1);
      }
      $scope.sortData=function(str)
      {
        $scope.activeHover=str;
        if(str=="friends")
        {
          $scope.getData.sort(function(a, b) {
              return parseFloat(a.twubric.friends) - parseFloat(b.twubric.friends);
          });
          if($scope.flagFriends==false)
          {
            $scope.getData.reverse();
            $scope.flagFriends=true;
          }
          else
          {
            $scope.flagFriends=false;
          }

    //    var d = new Date(1355270400);
  
       }
       else if(str=="chirpiness")
        {
          $scope.getData.sort(function(a, b) {
              return parseFloat(a.twubric.chirpiness) - parseFloat(b.twubric.chirpiness);
          });
          if($scope.flagChirpiness==false)
          {
            $scope.getData.reverse();
            $scope.flagChirpiness=true;
          }
          else
          {
            $scope.flagChirpiness=false;
          }
       }
       else if(str="influence")
       {
        $scope.getData.sort(function(a, b) {
              return parseFloat(a.twubric.influence) - parseFloat(b.twubric.influence);
          });
          if($scope.flagInfluence==false)
          {
            $scope.getData.reverse();
            $scope.flagInfluence=true;
          }
          else
          {
            $scope.flagInfluence=false;
          }
       }
       else if(str="total")
       {
        $scope.getData.sort(function(a, b) {
              return parseFloat(a.twubric.total) - parseFloat(b.twubric.total);
          });
          if($scope.flagTotal==false)
          {
            $scope.getData.reverse();
            $scope.flagTotal=true;
          }
          else
          {
            $scope.flagTotal=false;
          }
       }

      }




});


